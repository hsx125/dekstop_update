<?php

function alret_data($isi, $link)
{
	echo "<script>alert('".$isi."');</script>";
	echo "<meta http-equiv='refresh' content='0;URL=".$link."'>";
	die();
}
function cek_post($data, $link)
{
	// print_r($data);
	// die();
	foreach ($data as $key => $value) {
		if ($value=='') {
			$info .= '- '.$key.' Tidak Boleh Kosong \n';
		}
	}
	if ($info<>'') {
		echo "<script>alert('".$info."');</script>";
		echo "<meta http-equiv='refresh' content='0;URL=".$link."'>";
		die();
	}
}


function money($string, $koma=true, $null = '0'){		
	$string = number_format($string, 2, '.', ',');
	
	if( preg_match('/./', $string) ){
		$ex = explode('.', $string);
		$string = $ex[0];
	}
	
	$string = str_replace(',', '', $string);
	
	if($koma === false)
		$money = number_format($string, 0, ',', '.');
	else
		$money = number_format($string, 2, ',', '.');
	
	if( $money == 0 )
		$money = $null;		
	
	return($money);
}

function Indodate($sqldate, $delim = ' ') {
	$arMon = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    $arMonsrt = array("", "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agus", "Sep", "Okt", "Nov", "Des");
    $intBln = substr($sqldate, 5, 2); 
    $intMon = (int) $intBln;
    $bln = $arMonsrt[$intMon];

    return substr($sqldate, 8, 2) . $delim . $bln . $delim . substr($sqldate, 0, 4);
}

function IndodateTime($sqldate, $delim = ' ') {
	$arMon = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    $arMonsrt = array("", "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agus", "Sep", "Okt", "Nov", "Des");
    $intBln = substr($sqldate, 5, 2); 
    $intMon = (int) $intBln;
    $bln = $arMonsrt[$intMon];

    return substr($sqldate, 8, 2) . $delim . $bln . $delim . substr($sqldate, 0, 4);
}
 