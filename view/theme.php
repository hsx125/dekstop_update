
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?= APP ?>&nbsp;</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?= BASE_URL ?>/logo.png">

    <script src="<?= BASE_URL ?>/view/assets/js/jquery.js"></script>
    <script src="<?= BASE_URL ?>/view/assets/js/jquery-1.8.3.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="<?= BASE_URL ?>/view/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?= BASE_URL ?>/view/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/view/assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/view/assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/view/assets/lineicons/style.css">    
    <link href="<?= BASE_URL ?>/view/assets/js/fancybox/jquery.fancybox.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?= BASE_URL ?>/view/assets/css/style.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/view/assets/css/color.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/view/assets/css/custom.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/view/assets/css/style-responsive.css" rel="stylesheet">

    <script src="<?= BASE_URL ?>/view/assets/js/chart-master/Chart.js"></script>
    <link href="<?= BASE_URL ?>/view/assets/plugin/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      .text-middle{
        vertical-align: middle;
      }
    </style>
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo"><b><?= APP ?>&nbsp;</b></a>
            <!--logo end-->
            
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="?mode=admin&aksi=logout">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                 
                  <li class="mt">
                      <a href="?menu=home">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                  <?php include 'menu.php'; ?>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
            <?php
              if (isset($_GET['menu'])) {
                  $menu = $_GET['menu'];
                  include 'control/'.$_GET['menu'].'.php';
              }else{
                include 'control/home.php';
              }
            ?>
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <!-- <footer class="site-footer">
          <div class="text-center">
              2014 - Alvarez.is
              <a href="index.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer> -->
      <!--footer end-->
  </section>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header bg-red">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Konfirmasi Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda Yakin akan menghapus data ini.</p>               
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="#" class="btn btn-danger danger">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
        })
    </script>  
    <!-- js placed at the end of the document so the pages load faster -->

    <script src="<?= BASE_URL ?>/view/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?= BASE_URL ?>/view/assets/js/jquery.dcjqaccordion.2.7.js"></script>

    <script src="<?= BASE_URL ?>/view/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?= BASE_URL ?>/view/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?= BASE_URL ?>/view/assets/js/jquery.sparkline.js"></script>

    <script type="text/javascript" src="<?= BASE_URL ?>/view/assets/plugin/datetimepicker/moment.js" charset="UTF-8"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>/view/assets/plugin/datetimepicker/bootstrap-datetimepicker.js" charset="UTF-8"></script>

    <!--common script for all pages-->
    <script src="<?= BASE_URL ?>/view/assets/js/common-scripts.js"></script>
    <script src="<?= BASE_URL ?>/view/assets/js/fancybox/jquery.fancybox.js"></script>    
    
    <script src="<?= BASE_URL ?>/view/assets/js/bootstrap-switch.js"></script>

    <script type="text/javascript" src="<?= BASE_URL ?>/view/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>/view/assets/js/gritter-conf.js"></script>
    <script type="text/javascript">
         $(function () {
            $('.date_tahun').datetimepicker({
                viewMode: 'years',
                format: 'YYYY-MM-D'
            });
            $('.date_lengkap').datetimepicker({
              format: 'YYYY-MM-D'
            });
        });
      $(document).ready(function() {
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })
        $(function() {
        //    fancybox
          jQuery(".fancybox").fancybox();
        });
           $('.cekinput').keyup(function (){
                var kode = $(this).val();
                if (kode=='') {
                  $('.i_cekinput').addClass('has-error');
                  $('.i_cekinput').removeClass('has-success');
                } else {
                  $('.i_cekinput').addClass('has-success');
                  $('.i_cekinput').removeClass('has-error');
                }                
            });
                   
      });
    </script>
  </body>
</html>
