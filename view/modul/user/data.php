<?php
    $btn_aktif = $data =  false;
    $query = $mysqli->query("select * from user ORDER BY id");
    $no=1;
    while($dt_kategori = $query->fetch_array(MYSQLI_ASSOC)){
        if ($dt_kategori['akses']=='1') {
            $ket = 'ADMINISTRATOR';
        }else{
            $ket = 'OPERATOR';
        }
    	$data .= '<tr>
                    <td class="text-center">'.$no.'</td>
                    <td class="text-center">'.$dt_kategori['uname'].'</td>
                    <td class="text-center">'.$ket.'</td>
                    <td class="text-center">
                        <a class="btn btn-primary btn-xs" href="?menu='.$_GET['menu'].'&aksi=edit&amp;id='.$dt_kategori['id'].'">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a class="btn btn-danger btn-xs" data-href="?menu='.$_GET['menu'].'&aksi=hapus&amp;id='.$dt_kategori['id'].'"  data-toggle="modal" data-target="#confirm-delete" title="Hapus">
                            <i class="fa fa-trash-o"></i> 
                        </a>
                    </td>
                </tr>';
                $no++;
    }
?>
<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <h4><i class="fa fa-angle-right"></i> Data User</h4>
            <hr>
            <a class="mar-10 btn btn-info" href="?menu=<?= $_GET['menu'] ?>&aksi=tambah"><i class="fa fa-plus"></i> Tambah</a>
            <table class="table table-striped table-advance table-hover">
                <thead class="bg-hijau text-white text-center">
                    <tr>
                        <th class="text-center" width="5px"> No</th>
                        <th class="text-center"> Username</th>
                        <th class="text-center"> Akses</th>
                        <th class="text-center"> Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?= $data ?>
              </tbody>
          </table>
      </div><!-- /content-panel -->
  </div><!-- /col-md-12 -->
</div><!-- /row -->

<div class="modal fade" id="modal-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <div class="modal-header bg-red">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ?</p>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" class="btn btn-info danger">OKE</a>
            </div>
        </div>
    </div>
</div>

<script>
    $('#modal-status').on('show.bs.modal', function(e) {
        $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
    })
</script>  