            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                      <h4 class="mb"><i class="fa fa-angle-right"></i> Tambah User</h4>
                      <form class="form-horizontal style-form" action="?menu=<?= $_GET['menu']?>&aksi=simpan" method="post">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Username</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="user">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Password</label>
                              <div class="col-sm-10">
                                  <input type="password" class="form-control" name="pass">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Akses</label>
                              <div class="col-sm-10">
                                <select class="form-control" name="akses">
                                  <option value="">Pilih Akses</option>
                                  <option value="1">Administrator</option>
                                  <option value="2">Operator</option>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <button class="btn btn-primary">Simpan</button>
                              <a href="?menu=<?= $_GET['menu']?>&aksi=data" class="btn btn-danger">Kembali</a>
                            </div>
                          </div>
                      </form>
                  </div>
                </div><!-- col-lg-12-->         
            </div><!-- /row -->