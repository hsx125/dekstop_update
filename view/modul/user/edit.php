<?php
// die("SELECT * from kategori WHERE kode_kategori='".$_GET['id']."' ");
    $query = $mysqli->query("SELECT * from user WHERE id='".$_GET['id']."' ");
    $dt = $query->fetch_array(MYSQLI_ASSOC);
    $id = $dt['id'];
    $user = $dt['uname'];
    $pass = $dt['pswd'];
    $akses = $dt['akses'];

    $akses = array('1' => 'Administrator' , 
                '2' => 'Operator' );
    foreach ($akses as $key_akses => $value_akses) {
        $sel_akses = ($dt['akses']==$key_akses) ? 'selected' : '' ;
        $akses .= '<option '.$sel_akses.' value="'.$key_akses.'">'.$value_akses.'</option>';
    }
?>
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                      <h4 class="mb"><i class="fa fa-angle-right"></i> Edit User</h4>
                      <form class="form-horizontal style-form" action="?menu=<?= $_GET['menu'] ?>&aksi=update" method="post">
                        <input type="hidden" name="kode" value="<?= $id ?>">
                        <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Username</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="user" value="<?= $user ?>">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Akses</label>
                              <div class="col-sm-10">
                                <select class="form-control" name="akses">
                                  <option value="">Pilih Akses</option>
                                  <?= $akses ?>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Password Lama</label>
                              <div class="col-sm-10">
                                  <input type="password" class="form-control" name="pass_lama" >
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Password Baru</label>
                              <div class="col-sm-10">
                                  <input type="password" class="form-control" name="pass_baru" >
                              </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <button class="btn btn-primary">Simpan</button>
                              <a href="?menu=<?= $_GET['menu'] ?>&aksi=data" class="btn btn-danger">Kembali</a>
                            </div>
                          </div>
                      </form>
                  </div>
                </div><!-- col-lg-12-->         
            </div><!-- /row -->