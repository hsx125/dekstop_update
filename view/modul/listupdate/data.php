<!--
   Digunakan untuk mengelola data Arsip 
-->


<?php
    $btn_aktif = $data =  false; 
    $query=$mysqli->query("select * from version where version <> '-' AND aplikasi='".$_GET['app']."' order by id desc");
    $no=1;
    while($dt=$query->fetch_array(MYSQLI_ASSOC)){

    	$data .= '<tr>
                    <td class="text-center">'.$no.'</td>                    
                    <td class="text-center">'.$dt['version'].'</td>  
                    <td class="text-center">'.($dt['tanggal']).'</td>
                    <td class="text-left">'.$dt['filename'].'</td>     
                    <td class="text-left">'.$dt['ket'].'</td>
                    <td class="text-center"> 
                        <a class="btn btn-danger btn-xs" data-href="?menu='.$_GET['menu'].'&app='.$_GET['app'].'&aksi=hapus&amp;id='.$dt['id'].'"  data-toggle="modal" data-target="#confirm-delete" title="Hapus">
                            <i class="fa fa-trash-o"></i> 
                        </a>
                    </td>
                      
                </tr>';
                $no++;
    }
?>
<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <h4><i class="fa fa-angle-right"></i>List Patch Update</h4>
            <hr>
                 
            <a class="mar-10 btn btn-info" href="?menu=<?= $_GET['menu'] ?>&aksi=tambah&app=<?= $_GET['app'] ?>"><i class="fa fa-plus"></i> Tambah</a>
            <table class="table table-striped  table-bordered table-advance table-hover">
                <thead class="bg-hijau text-white text-center">
                    <tr>
                        <th class="text-center" width="5px"> No</th>
                        <th class="text-center"> Versi Patch</th>
                        <th class="text-center" style="width: 14%"> Tanggal Upload</th>
                        <th class="text-center"> Nama File Patch</th>
                        <th class="text-center"> Keterangan</th> 
                        <th class="text-center" width="80px" > Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?= $data ?>
              </tbody>
          </table>
      </div><!-- /content-panel -->
  </div><!-- /col-md-12 -->
</div><!-- /row -->

<div class="modal fade" id="modal-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <div class="modal-header bg-red">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ?</p>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" class="btn btn-info danger">OKE</a>
            </div>
        </div>
    </div>
</div>

<script>
    $('#modal-status').on('show.bs.modal', function(e) {
        $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
    })
    $(document).ready(function() {
        $('#filter_btn').click(function (){
            var data = $('#data').val();
            window.location.href='?menu=<?=  $_GET['menu'] ?>&aksi=data&dt='+data;                   
        });
                   
      });
</script>  

