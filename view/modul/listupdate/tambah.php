<!--
   Digunakan untuk tambah data Arsip 
-->
  
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                      <h4 class="mb"><i class="fa fa-angle-right"></i> Tambah Patch Update</h4>
                      <form class="form-horizontal style-form" action="?menu=<?= $_GET['menu']?>&aksi=simpan&app=<?= $_GET['app'] ?>" method="post" enctype="multipart/form-data">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Patch Versi</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="versi">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Keterangan Patch</label>
                              <div class="col-sm-10">
                                  <textarea class="form-control" rows="5" name="ket"></textarea> 
                              </div>
                          </div> 

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">File Patch</label>
                              <div class="col-sm-10">
                                  <input type="file" class="form-control" name="doc" accept=".rar">
                              </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <button class="btn btn-primary">Simpan</button>
                              <a href="?menu=<?= $_GET['menu']?>&aksi=data" class="btn btn-danger">Kembali</a>
                            </div>
                          </div>
                      </form>
                  </div>
                </div><!-- col-lg-12-->         
            </div><!-- /row -->