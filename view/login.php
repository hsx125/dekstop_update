<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?= APP ?>&nbsp;</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= BASE_URL ?>/logo.png">

    <!-- Bootstrap core CSS -->
    <link href="<?= BASE_URL ?>/view/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?= BASE_URL ?>/view/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?= BASE_URL ?>/view/assets/css/style.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/view/assets/css/style-responsive.css" rel="stylesheet">
    <script src="<?= BASE_URL ?>/view/assets/js/jquery.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login form-horizontal style-form" method="post" action="?mode=admin&aksi=login" >
		        <h2 class="form-login-heading">sign in now</h2>
            <div class="login-wrap text-center">
              <p class="centered"><a href="#"><img src="<?= BASE_URL ?>/logo.png" class="" width="40%"></a></p>
               <b><?= APP ?></b>
             
            </div>
		        <div class="login-wrap">
		            <input name="user" type="text" class="form-control required" placeholder="Username" autofocus>
		            <br>
		            <input name="pass" type="password" class="form-control required" placeholder="Password">
		            <br>
		            <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
		            <hr>
		            
		            <!-- <div class="login-social-link centered">
		            <p>or you can sign in via your social network</p>
		                <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
		                <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>
		            </div>
		            <div class="registration">
		                Don't have an account yet?<br/>
		                <a class="" href="#">
		                    Create an account
		                </a>
		            </div> -->
		
		        </div>
		
		         
		      </form>	  	
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?= BASE_URL ?>/view/assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?= BASE_URL ?>/view/assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?= BASE_URL ?>/view/assets/img/bg_login.png", {speed: 500});
    </script>

    
    <script type="text/javascript" src="<?= BASE_URL ?>/view/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>/view/assets/js/gritter-conf.js"></script>


  </body>
</html>
