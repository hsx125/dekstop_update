<!--
   Digunakan untuk mengelola data Arsip 
-->

<h3><i class="fa fa-angle-right"></i> List Update <?= str_replace('_', ' ', $_GET['app']) ?></h3>
<?php
$aksi = $_GET['aksi'];
	if (isset($aksi)) {
		if ($aksi=='data') {
			include ROOT_DIR.'/view/modul/'.$_GET['menu'].'/data.php';
		}else if ($aksi=='tambah') {
			include ROOT_DIR.'/view/modul/'.$_GET['menu'].'/tambah.php';
		}else if ($aksi=='simpan') {
			simpan();
		}else if ($aksi=='hapus') {
			hapus();
		}else{
			echo "<script>alert('Menu Bemum Ada !!!!!!!!!');</script>";
			echo "<meta http-equiv='refresh' content='0;URL=".BASE_URL."?menu=".$_GET['menu']."&app=".$_GET['app']."&aksi=data'>";
			die();
			// include ROOT_DIR.'/view/modul/'.$_GET['menu'].'/data.php';
		}
		// die('ASDF');
	}else{
		// die('asdfg');
		include ROOT_DIR.'/view/modul/'.$_GET['menu'].'/data.php';

	} 

// Proses Simpan Ke Tabel Arsip
function simpan(){
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATA);

	$cek = array('Versi' => $_POST['versi'], 
				'Keterangan' => $_POST['ket'],
				'File Update' => $_FILES['doc']['name']  
				);
    $link = BASE_URL.'?menu='.$_GET['menu'].'&app='.$_GET['app'].'&aksi=tambah';
	cek_post($cek, $link);

	if ($_FILES['doc']['name']) {
		$ex_fto = explode('.', $_FILES['doc']['name']);
		$nama_file = date('Ymyhis').'.'.$ex_fto[1];
		// die($nama_file);

		$target_dir = DOCROOT."files/".$_GET['app']."/";
		if (!file_exists($target_dir)) {
		    mkdir(DOCROOT."files/" . $_GET['app'], 0777);
		} 
		$target_file = $target_dir . $nama_file; 
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	    move_uploaded_file($_FILES["doc"]["tmp_name"], $target_file);
	    include DOCROOT.'config.php';
	    
	}
	$query = "INSERT INTO version(aplikasi, version, ket, filename)
						VALUES('".$_GET['app']."', TRIM('".$_POST['versi']."'), '".$_POST['ket']."', '".$nama_file."')";
						// die($query);
	$act = $mysqli->query($query);
	if ($act) {
		$isi_sukses = 'Data Berhasil Disimpan !!';
		$link_sukses = BASE_URL."?menu=".$_GET['menu']."&app=".$_GET['app']."&aksi=data";
    	alret_data($isi_sukses, $link_sukses); 
		
	} else {
		$isi_gagal = 'Data Gagal Disimpan !!';
		$link_gagal = BASE_URL."?menu=".$_GET['menu']."&app=".$_GET['app']."&aksi=tambah";
    	alret_data($isi_gagal, $link_gagal);
		
	}
	
}

// Hapus Data Arsip

function hapus(){
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATA);

	$query = "DELETE FROM version WHERE id='".$_GET['id']."'";
	$act = $mysqli->query($query);

	$link = BASE_URL."?menu=".$_GET['menu']."&app=".$_GET['app']."&aksi=data";		
	if ($act) {
		$isi_sukses = 'Data Berhasil Dihapus !!';
    	alret_data($isi_sukses, $link); 
		
	} else {
		$isi_gagal = 'Data Gagal Dihapus !!';
    	alret_data($isi_gagal, $link);
		
	}

}


?>
